# Analyse de données du projet Cassiopea - stage M1 Camille ABRAN

[![MAJ](https://img.shields.io/badge/mise%96à%96jour-08%2F04%2F2024-red?labelColor=000000)]()
[![R](https://img.shields.io/badge/R-4.3.3-23aa62.svg?labelColor=000000)](https://www.nextflow.io/)
[![Developer](https://img.shields.io/badge/Developer-Camille%20ABRAN-yellow?labelColor=000000)](https://sebimer.ifremer.fr/)


## Introduction

Ce répertoire Git contiendra mes scripts pour l'analyse de données Cassiope pour mon stage de M1 Génomique Environnementale du 02/04/2024 au 24/05/2024.

## Crédits

L'ensemble des codes ont été écrit par Camille ABRAN lors de son stage au seins du [SeBiMER](https://sebimer.ifremer.fr).

